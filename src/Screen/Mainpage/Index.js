import React from "react";
import {
  Text,
  ImageBackground,
  StyleSheet,
  View,
  FlatList,
  Dimensions,
  TouchableOpacity,
} from "react-native";
import axios from "axios";
//homeScreenDetails
// let dataList = [
//   //   { key: "1", color: "purple", text: "Bank Details" },
//   //   { key: "2", color: "blue", text: "Account Details" },
//   //   { key: "3", color: "darkgreen", text: "Insurance" },
//   //   { key: "4", color: "purple", text: "NOC" },
//   //   { key: "5", color: "brown", text: "Payments" },
// ];

const numColumns = 2;

const WIDTH = Dimensions.get("window").width;

export default class Mainpage extends React.Component {
  constructor() {
    super();
    this.state = { dataList: [] };
  }

  async getHomeScreenData() {
    const res = await axios.get(
      "http://school360apps.azurewebsites.net/school360apps/index.php/AssettrackerAttLogin/homeScreenDetails"
    );

    if (res.data.status === "Success") {
      this.setState({ dataList: res.data.data });
    }
  }

  componentDidMount() {
    this.getHomeScreenData();
  }

  formatData = (dataList, numColumns) => {
    const totalRows = Math.floor(dataList.length / numColumns);
    let totalLastRow = dataList.length - totalRows * numColumns;

    while (totalLastRow !== 0 && totalLastRow !== numColumns) {
      dataList.push({ key: "blank", empty: true });
      totalLastRow++;
    }

    return dataList;
  };

  _renderItem = ({ item, index }) => {
    if (item.empty) {
      return (
        <View
          style={[
            styles.ItemStyle,
            {
              backgroundColor: item.color,
            },
          ]}
        />
      );
    }
    return (
      <TouchableOpacity
        style={[
          styles.ItemStyle,
          {
            backgroundColor: item.color,
          },
        ]}
      >
        <View>
          <Text style={styles.itemText}>{item.text}</Text>
        </View>
      </TouchableOpacity>
    );
  };

  render() {
    return (
      <View
        style={{
          flex: 1,
          paddingTop: 40,
        }}
      >
        <FlatList
          data={this.formatData(this.state.dataList, numColumns)}
          renderItem={this._renderItem}
          keyExtractor={(item, index) => index.toString()}
          numColumns={numColumns}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  itemText: {
    color: "#fff",
    fontSize: 20,
    textAlign: "center",
  },
  ItemStyle: {
    alignItems: "center",
    justifyContent: "center",
    height: 100,
    flex: 1,
    margin: 1,
    height: WIDTH / numColumns,
  },
});
