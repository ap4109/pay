import { StyleSheet } from 'react-native';
const Styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-around'
    },
    logoView: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
    },
    logoStyle: {
        resizeMode: 'contain',
        height: 120,
        width: 120,
    },
    logoText: {
        fontSize: 25,
        paddingBottom: 20,
        color: 'green'
    },
    textInputView: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        justifyContent: 'center',
        padding: 10,
        margin: 10
    },
    nameStyle: {
        fontSize: 20,
        height: 40,
        borderWidth: 1,
        width: 300,
        borderRadius: 10,
        marginBottom: 10
    },
    passwordStyle: {
        fontSize: 20,
        height: 40,
        borderWidth: 1,
        width: 300,
        borderRadius: 10
    },
    touchView: {
        alignItems: 'center'
    },
    loginStyle: {
        borderWidth: 1,
        fontSize: 30,
        fontWeight: 'bold',
        color: 'white',
        backgroundColor: 'green',
        borderColor: 'green',
        borderRadius: 5,
        width: 350,
        height: 60,
        textAlign: 'center'
    },
    bottomView: {
        alignSelf: 'flex-start',
        margin: 20
    },
    registerView: {
        flexDirection: 'row'
    },
    text1: {
        fontSize: 25
    },
    text2: {
        fontSize: 25,
        color: 'green'
    },
    forgetPin: {
        flexDirection: 'row',
        fontSize:25
    },
}
)

export default Styles;
