import React from "react";
import {
  TextInput,
  View,
  Image,
  Text,
  TouchableOpacity,
  ImageBackground,
} from "react-native";
import Styles from "./Styles";
import axios from "axios";
export default class Login extends React.Component {
  constructor() {
    super();
    this.state = { isLoggedin: false, username: "", password: "" };
  }

  async loginCheck() {
    const res = await axios.post(
      "http://school360apps.azurewebsites.net/school360apps/index.php/AssettrackerAttLogin/checkuserPay",
      {
        username: this.state.username,
        password: this.state.password,
      }
    );
    if (res.data.status === "Success") {
      this.props.navigation.navigate("MainScreen");
    }
  }

  render() {
    return (
      <ImageBackground
        source={require("../../Image/gradient.png")}
        style={Styles.container}
      >
        <View style={Styles.logoView}>
          <Image
            source={require("../../Image/logo.png")}
            style={Styles.logoStyle}
          />
          <Text style={Styles.logoText}>O-Pay</Text>
        </View>
        <View style={Styles.textInputView}>
          <TextInput
            placeholder="User name"
            placeholderTextColor="#000000"
            style={Styles.nameStyle}
            onChangeText={(text) => {
              this.setState({ username: text });
            }}
            value={this.state.username}
          />
          <TextInput
            placeholder="Password"
            placeholderTextColor="#000000"
            style={Styles.passwordStyle}
            onChangeText={(text) => {
              this.setState({ password: text });
            }}
            value={this.state.password}
          />
        </View>
        <View style={Styles.touchView}>
          <TouchableOpacity onPress={() => this.loginCheck()}>
            <Text style={Styles.loginStyle}>Login</Text>
          </TouchableOpacity>
        </View>
        <View style={Styles.bottomView}>
          <View style={Styles.registerView}>
            <Text style={Styles.text1}>Not registered ?</Text>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("Register")}
            >
              <Text style={Styles.text2}> Register</Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity>
            <Text style={Styles.forgetPin}>Forget Password</Text>
          </TouchableOpacity>
        </View>
      </ImageBackground>
    );
  }
}
