import React from "react";
import {
  Text,
  View,
  ImageBackground,
  TextInput,
  TouchableOpacity,
  Image,
} from "react-native";
import Styles from "./Styles";
import axios from "axios";

export default class Registration extends React.Component {
  constructor() {
    super();
    this.state = { username: "", password: "", email: "", mobile: "" };
  }

  async RegisterUser() {
    const res = await axios.post(
      "http://school360apps.azurewebsites.net/school360apps/index.php/AssettrackerAttLogin/adduserPay",
      {
        username: this.state.username,
        password: this.state.password,
        email: this.state.email,
        mobile: this.state.mobile,
      }
    );
    if (res.data.status === "Success") {
      this.props.navigation.navigate("Home");
    }
  }

  render() {
    return (
      <ImageBackground
        source={require("../../Image/gradient.png")}
        style={Styles.container}
      >
        <View style={Styles.logoView}>
          <Image
            source={require("../../Image/logo.png")}
            style={Styles.logoStyle}
          />
          <Text style={Styles.logoText}>O-Pay</Text>
        </View>
        <View>
          <TextInput
            placeholder="Enter Your Name"
            placeholderTextColor="#000000"
            style={Styles.textInput}
            onChangeText={(text) => {
              this.setState({ username: text });
            }}
            value={this.state.username}
          />
          <TextInput
            placeholder="Enter Your Email id"
            placeholderTextColor="#000000"
            style={Styles.textInput}
            onChangeText={(text) => {
              this.setState({ email: text });
            }}
            value={this.state.email}
          />
          <TextInput
            placeholder="Enter Your Mobile number"
            placeholderTextColor="#000000"
            style={Styles.textInput}
            onChangeText={(text) => {
              this.setState({ mobile: text });
            }}
            value={this.state.mobile}
          />
          <TextInput
            placeholder="Enter Your Password"
            placeholderTextColor="#000000"
            style={Styles.textInput}
            onChangeText={(text) => {
              this.setState({ password: text });
            }}
            value={this.state.password}
          />
        </View>
        <View style={Styles.touchView}>
          <TouchableOpacity onPress={() => this.RegisterUser()}>
            <Text style={Styles.touchText}>Register</Text>
          </TouchableOpacity>
        </View>
      </ImageBackground>
    );
  }
}
