import { StyleSheet } from 'react-native';
const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ccff66',
        flexDirection: 'column',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    logoView: {
        alignItems: 'center',
        justifyContent: 'center',

    },
    logoStyle: {
        resizeMode: 'contain',
        height: 120,
        width: 120,
    },
    logoText: {
        fontSize: 25,
        paddingBottom: 20,
        color: 'green'
    },
    textInput: {
        fontSize: 20,
        height: 40,
        borderWidth: 1,
        width: 300,
        borderRadius: 10,
        margin: 20
    },
    touchView: {
        justifyContent: 'center',
        flexDirection: 'row'
    },
    touchText: {
        borderWidth: 1,
        fontSize: 30,
        fontWeight: 'bold',
        color: 'white',
        backgroundColor: 'green',
        borderColor: 'green',
        borderRadius: 5,
        width: 350,
        height: 60,
        textAlign: 'center'
    }
}
)
export default Styles;
