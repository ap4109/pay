import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Login from './src/Screen/Login/Index';
import Registration from './src/Screen/Registration/Index'
import Mainpage from './src/Screen/Mainpage/Index'
const navigator = createStackNavigator({
  Home: {
    screen: Login,
    navigationOptions: {
      headerShown: false,
    }
  },
  Register: {
      screen: Registration,
      navigationOptions: {
          headerShown: false,
      }
  },
  MainScreen: {
      screen: Mainpage,
      navigationOptions: {
          headerShown: false,
      }
  }
}
)
export default createAppContainer(navigator);